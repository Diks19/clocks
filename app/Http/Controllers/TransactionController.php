<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Total;
use App\Models\Address;
use App\Models\Product;
use App\Models\Delivery;
use App\Models\AddressType;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Models\PaymentMethod;
use App\Models\ProductDetails;
use App\Models\TransactionDetails;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.transaction.checkout', [
            'atypes' => AddressType::all(),
            'payments' => PaymentMethod::all(),
            'deliveries' => Delivery::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Cart $cart, ProductDetails $productDetails)
    {
        // VALIDATION
        if ($request->payment_method == 1) {
            if ($request->address == 0) {
                return response()->json([
                    'message' => 'Select address first!'
                ]);
            } else if ($request->delivery == 0) {
                return response()->json([
                    'message' => 'Select the expedition!'
                ]);
            } else if ($cart->count() == null) {
                return response()->json([
                    'message' => 'Your cart is empty',
                ]);
            } else if ($request->pay >= Total::total()) {
                $return = $request->pay - Total::total();
            }
        } else {
            if ($request->address == 0) {
                return response()->json([
                    'message' => 'Select address first!'
                ]);
            } else if ($request->delivery == 0) {
                return response()->json([
                    'message' => 'Select the expedition!'
                ]);
            } else if ($request->pay < Total::total()) {
                return response()->json([
                    'message' => 'Your amount of pay is not enough!',
                ]);
            } else if ($cart->count() == null) {
                return response()->json([
                    'message' => 'Your cart is empty',
                ]);
            } else if ($request->pay >= Total::total()) {
                $return = $request->pay - Total::total();
            }
        }
        // MAKE INVOCE
        function invoice()
        {
            $angka = [];
            for ($i = 0; $i <= 9; $i++) {
                $angka_random = rand(0, 9);
                array_push($angka, $angka_random);
            }
            $angka = join('', $angka);
            return 'INV' . $angka;
        }
        // TRANSACTION
        $transaction = Transaction::create([
            'user_id' => $request->user,
            'address_id' => $request->address,
            'delivery_id' => $request->delivery,
            'payment_method_id' => $request->delivery,
            'no_invoice' => invoice(),
            'buyers_pay' => $request->pay,
            'total_payment' => Total::total(),
            'returns' => $return,
        ]);

        // TRANSACTION DETAILS
        $carts = Cart::where('user_id',  Auth::user()->id)->get();
        foreach ($carts as $cartss) {
            TransactionDetails::create([
                'transaction_id' => $transaction->id,
                'product_id' => $cartss->product_id,
                'date' => date('Y-m-d'),
                'quantity' => $cartss->qty,
                'total_price' => $cartss->price_total,
            ]);
        }
        $carts = DB::table('carts')->where('user_id', Auth::user()->id)->get();
        foreach ($carts as $cartss) {
            $cartss->product_id;
            $asd = $productDetails->where('product_id', $cartss->product_id)->first();
            $asd->update([
                'stock' =>  $asd->stock - $cartss->qty
            ]);
        }
        $carts = DB::table('carts')->where('user_id', Auth::user()->id);
        $carts->delete();
        return response()->json([
            'message' => 'Transaksi berhasil!',
            'header' => 200
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
