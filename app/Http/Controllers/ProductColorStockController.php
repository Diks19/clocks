<?php

namespace App\Http\Controllers;

use App\Models\ProductColorStock;
use Illuminate\Http\Request;

class ProductColorStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductColorStock  $productColorStock
     * @return \Illuminate\Http\Response
     */
    public function show(ProductColorStock $productColorStock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductColorStock  $productColorStock
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductColorStock $productColorStock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductColorStock  $productColorStock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductColorStock $productColorStock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductColorStock  $productColorStock
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductColorStock $productColorStock)
    {
        //
    }
}
