<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Address;

class AddressController extends Controller
{
    public function store(Request $request, Address $address)
    {
        if ($request->province != 0 && $request->city != 0) {
            $data = Address::create([
                'user_id' => $request->user,
                'address_types_id' => $request->atype,
                'full_name' => $request->name,
                'phone_number' => $request->number,
                'house_number' => $request->house,
                'city' => $request->city,
                'postal_code' => $request->postal,
                'province' => $request->province,
            ]);
            return response()->json([
                'data' => $data,
            ]);
        }
        // return response()->json([
            
        // ])
    }
}
