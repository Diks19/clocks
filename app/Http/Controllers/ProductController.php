<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Type;
use App\Models\Brand;
use App\Models\Total;
use App\Models\Product;
use App\Models\ProductColor;
use Illuminate\Http\Request;
use App\Models\ProductColorDetailsPivot;
use App\Models\ProductDetails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.product.list', [
            'products' => Product::with(['type', 'brand', 'detail'])->latest()->paginate(3),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.product.form', [
            'colors' => ProductColor::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_name' => 'required',
            'brand_id' => 'required',
            'type_id' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'description' => 'required',
            'product_image' => 'image|file|mimes:png,jpg,jpeg|max:2048'
        ]);
        $data = [
            'product_name' => $request->product_name,
            'brand_id' => $request->brand_id,
            'type_id' => $request->type_id
        ];
        $detail = [
            'price' => $request->price,
            'stock' => $request->stock,
            'description' => $request->description,
            'product_image' => $request->product_image
        ];
        if ($request->hasFile('product_image')) {
            $detail['product_image'] = $request->file('product_image')->store('product-image');
        } else {
            $detail['product_image'] = 'product-image/NoImg.png';
        };
        $productCreate = Product::create($data);
        $productDetailsCreate = ProductDetails::create([
            'product_id' => $productCreate->id,
            'price' => $request->price,
            'stock' => $request->stock,
            'weight' => 1000,
            'description' => $request->description,
            'product_image' => $detail['product_image']
        ]);
        $colors = $request->color;
        foreach ($colors as $color) {
            ProductColorDetailsPivot::create([
                'product_details_id' => $productDetailsCreate->id,
                'product_color_id' => $color,
            ]);
        }
        return redirect(route('product.index'))->with('success', 'Successfully Added Product');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('admin.product.detail', [
            'product' => $product->find($product->id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('admin.product.form', [
            'product' => $product,
            'colors' => ProductColor::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'product_name' => 'required',
            'brand_id' => 'required',
            'type_id' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'description' => 'required',
            'product_image' => 'image|file|mimes:png,jpg,jpeg|max:2048'
        ]);
        $data = [
            'product_name' => $request->product_name,
            'brand_id' => $request->brand_id,
            'type_id' => $request->type_id
        ];
        $detail = [
            'price' => $request->price,
            'stock' => $request->stock,
            'description' => $request->description,
            'product_image' => $request->product_image
        ];
        if ($request->hasFile('product_image')) {
            if ($request->old_product_image) {
                Storage::delete($request->old_product_image);
            }
            $detail['product_image'] = $request->file('product_image')->store('product-image');
        } else {
            if ($product->find($product->id)->detail->product_image === 'product-image/NoImg.png') {
                $detail['product_image'] = 'product-image/NoImg.png';
            } else {
                $detail['product_image'] = $product->find($product->id)->detail->product_image;
            }
        };
        $product->find($product->id)->update($data);
        ProductDetails::find($product->id)->update([
            'price' => $request->price,
            'stock' => $request->stock,
            'description' => $request->description,
            'product_image' => $detail['product_image']
        ]);
        return redirect(route('product.index'))->with('success', 'Successfully Update Product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, ProductDetails $productDetails, ProductColorDetailsPivot $productColorDetailsPivot)
    {
        $deletes = $productColorDetailsPivot->where('product_id', '=', $product->id)->get();
        if ($product->find($product->id)->detail->product_image !== 'product-image/NoImg.png') {
            Storage::delete($product->find($product->id)->detail->product_image);
        };
        $product->find($product->id)->delete();
        $productDetails->find($product->id)->delete();
        foreach ($deletes as $delete) {
            $delete->delete();
        };
        return redirect(route('product.index'))->with('success', 'Successfuly Delete a Product');
    }
}
