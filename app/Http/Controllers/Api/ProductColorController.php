<?php

namespace App\Http\Controllers\Api;

use App\Models\ProductColor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'message' => 'List of Color',
            'datas' => ProductColor::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'color_name' => 'required|unique:product_colors',
            'color_hex' => 'required|unique:product_colors'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'message' => 'Failed to add Color!'
            ]);
        }
        $data = ProductColor::create([
            'color_name' => $request->color_name,
            'color_hex' => $request->color_hex
        ]);
        return response()->json([
            'code' => 200,
            'message' => 'Success to add Color!',
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductColor  $productColor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'message' => 'Success finding data Color!',
            'data' => ProductColor::findOrFail($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductColor  $productColor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'color_name' => 'required',
            'color_hex' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'messsage' => 'Failed to update Color!'
            ]);
        }
        $color = ProductColor::find($id);
        $color->color_name = $request->color_name;
        $color->color_hex = $request->color_hex;
        $color->save();
        return response()->json([
            'code' => 200,
            'message' => 'Success to update Color!',
            'data' => $color
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductColor  $productColor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductColor::findOrFail($id)->delete();
        return response()->json([
            'message' => 'Success to delete Type!'
        ]);
    }
}
