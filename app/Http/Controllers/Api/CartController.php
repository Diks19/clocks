<?php

namespace App\Http\Controllers\Api;

use App\Models\Cart;
use App\Models\Total;
use Illuminate\Http\Request;
use App\Models\ProductDetails;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Cart $cart)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required',
            'user_id' => 'required',
            'color_id' => 'required',
            'qty' => 'required',
            'price_total' => 'required',
        ]);
        $cartes = $cart->all();
        $carts = $cartes
            ->where('product_id', '==', $request->product_id)
            ->where('user_id', '==', Auth::user()->id)
            ->where('color_id', '==', $request['color_id'])
            ->first();
        if ($carts) {
            $resultQty = $carts->qty += $request['qty'];
            $weight = ProductDetails::where('product_id', $request->product_id)->first();
            $finalWeight = intval($weight->weight) * intval($resultQty);
            $resultPriceTotal = $carts->price_total += $request['price_total'];
            $cart->where('id', $carts->id)->update([
                'final_weight' => $finalWeight,
                'qty' => $resultQty,
                'price_total' => $resultPriceTotal
            ]);
            return response()->json([
                'cartId' => $carts->id,
                'qty' => $resultQty,
                'total' => Total::total(),
            ]);
        } else {
            $weight = ProductDetails::where('product_id', $request->product_id)->first();
            $request['final_weight'] = intval($weight->weight) * intval($request->qty);
            $tes = Cart::create($request->all());
            $cartss = $tes->load(['product.detail', 'color']);
            return response()->json([
                'cart' => $cartss,
                'total' => Total::total(),
                'cart_count' => Cart::count(),
                'message' => 'Berhasil Menambah Ke Cart!'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        //
    }
}
