<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'message' => 'List of Type',
            'datas' => Type::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|unique:types|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'message' => 'Failed to add Type!'
            ]);
        }
        $data = Type::create([
            'type' => $request->type
        ]);
        return response()->json([
            'code' => 200,
            'message' => 'Success to add Type!',
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'message' => 'Success finding data Type!',
            'data' => Type::findOrFail($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'messsage' => 'Failed to update Type!'
            ]);
        }
        $type = $type->find($type->id);
        $type->type = $request->type;
        $type->save();
        return response()->json([
            'code' => 200,
            'message' => 'Success to update Type!',
            'data' => $type
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        $type->findOrFail($type->id)->delete($type);
        return response()->json([
            'message' => 'Success to delete Type!'
        ]);
    }
}
