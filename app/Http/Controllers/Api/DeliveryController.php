<?php

namespace App\Http\Controllers\Api;

use App\Models\Delivery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'message' => 'List of Delivery Expedition',
            'datas' => Delivery::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'expedition_name' => 'required|unique:deliveries|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'message' => 'Failed to add Expedition!'
            ]);
        }
        $data = Delivery::create([
            'expedition_name' => $request->expedition_name
        ]);
        return response()->json([
            'code' => 200,
            'message' => 'Success to add Expedition!',
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'message' => 'Success finding data Expedition!',
            'data' => Delivery::findOrFail($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Delivery $delivery)
    {
        $validator = Validator::make($request->all(), [
            'expedition_name' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'messsage' => 'Failed to update Expedition!'
            ]);
        }
        $delivery = $delivery->find($delivery->id);
        $delivery->expedition_name = $request->expedition_name;
        $delivery->save();
        return response()->json([
            'code' => 200,
            'message' => 'Success to update Expedition!',
            'data' => $delivery
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Delivery  $delivery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Delivery $delivery)
    {
        $delivery->findOrFail($delivery->id)->delete($delivery);
        return response()->json([
            'message' => 'Success to delete Expedition!'
        ]);
    }
}
