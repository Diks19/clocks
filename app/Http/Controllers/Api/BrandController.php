<?php

namespace App\Http\Controllers\Api;

use App\Models\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'message' => 'List of Brand',
            'datas' => Brand::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'brand_name' => 'required|unique:brands|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'message' => 'Failed to add Brand!'
            ]);
        }
        $data = Brand::create([
            'brand_name' => $request->brand_name
        ]);
        return response()->json([
            'code' => 200,
            'message' => 'Success to add Brand!',
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'message' => 'Success finding data Brand!',
            'data' => Brand::findOrFail($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $validator = Validator::make($request->all(), [
            'brand_name' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'messsage' => 'Failed to update Brand!'
            ]);
        }
        $brand = $brand->find($brand->id);
        $brand->brand_name = $request->brand_name;
        $brand->save();
        return response()->json([
            'code' => 200,
            'message' => 'Success to update Brand!',
            'data' => $brand
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        $brand->findOrFail($brand->id)->delete($brand);
        return response()->json([
            'message' => 'Success to delete Brand!'
        ]);
    }
}
