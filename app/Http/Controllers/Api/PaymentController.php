<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\PaymentMethod;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'message' => 'List of Payment Method',
            'datas' => PaymentMethod::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payment_method' => 'required|unique:payment_methods|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'message' => 'Failed to add Payment Method!'
            ]);
        }
        $data = PaymentMethod::create([
            'payment_method' => $request->payment_method
        ]);
        return response()->json([
            'code' => 200,
            'message' => 'Success to add Payment Method!',
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PaymentMethod  $paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'message' => 'Success finding data Payment Method!',
            'data' => PaymentMethod::findOrFail($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PaymentMethod  $paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'payment_method' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors(),
                'messsage' => 'Failed to update Payment Method!'
            ]);
        }
        $paymentMethod = PaymentMethod::find($id);
        $paymentMethod->payment_method = $request->payment_method;
        $paymentMethod->save();
        return response()->json([
            'code' => 200,
            'message' => 'Success to update Payment Method!',
            'data' => $paymentMethod
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PaymentMethod  $paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PaymentMethod::findOrFail($id)->delete();
        return response()->json([
            'message' => 'Success to delete Payment Method!'
        ]);
    }
}
