<?php

namespace App\Http\Controllers;

use App\Models\Type;
use App\Models\Brand;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\ProductColorDetailsPivot;
use App\Models\ProductDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SubProductController extends Controller
{
    public function productType(Type $type)
    {
        return view('admin.sub-product.type', [
            'products' => Product::where('type_id', $type->id)->latest()->paginate(3),
        ]);
    }
    public function productBrand(Brand $brand)
    {
        return view('admin.sub-product.brand', [
            'products' => Product::where('brand_id', $brand->id)->latest()->paginate(3),
        ]);
    }
    public function productColor(ProductColor $productColor, ProductDetails $productDetails)
    {
        // return view('admin.sub-product.color', [
        //     'products' => $products,

        // ]);
    }
}
