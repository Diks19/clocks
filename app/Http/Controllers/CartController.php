<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Total;
use Illuminate\Http\Request;
use App\Models\ProductDetails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Cart $cart)
    {
        
        $pId =  url()->previous();
        $pId = collect(mb_substr($pId, -1));
        $validatedData = $request->validate([
            'product_id' => 'required',
            'user_id' => 'required',
            'color_id' => 'required',
            'qty' => 'required',
            'price_total' => 'required',
        ]);
        $cartes = $cart->all();
        $carts = $cartes
            ->where('product_id', '==', $pId[0])
            ->where('user_id', '==', Auth::user()->id)
            ->where('color_id', '==', $validatedData['color_id'])
            ->first();
        if ($carts) {
            $resultQty = $carts->qty += $validatedData['qty'];
            $weight = ProductDetails::where('product_id', $pId[0])->first();
            $finalWeight = intval($weight->weight) * intval($resultQty);
            $resultPriceTotal = $carts->price_total += $validatedData['price_total'];
            $cart->where('id', $carts->id)->update([
                'final_weight' => $finalWeight,
                'qty' => $resultQty,
                'price_total' => $resultPriceTotal
            ]);
            return response()->json([
                'cartId' => $carts->id,
                'qty' => $resultQty,
                'total' => Total::total(),
            ]);
        } else {
            $weight = ProductDetails::where('product_id', $pId[0])->first();
            $validatedData['final_weight'] = intval($weight->weight) * intval($request->qty);
            $tes = Cart::create($validatedData);
            $cartss = $tes->load(['product.detail', 'color']);
            return response()->json([
                'cart' => $cartss,
                'total' => Total::total(),
                'cart_count' => Cart::count(),
                'message' => 'Berhasil Menambah Ke Cart!'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function show(Cart $cart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cart $cart)
    {
        $validatedData = $request->validate([
            'qty' => 'required',
        ]);
        $pId = $cart->where('id', $request->id)->first();
        $pId = $pId->product_id;
        $weight = ProductDetails::where('product_id', $pId)->first();
        $validatedData['final_weight'] = intval($weight->weight) * intval($request->qty);
        $priceTotal = $cart->find($request->id)->product->detail->price * $validatedData['qty'];
        $cart->find($request->id)->update($validatedData);
        $cart->find($request->id)->update(['price_total' => $priceTotal]);
        $cartss = $cart->find($request->id)->load(['product.detail', 'color']);
        return response()->json([
            'cart' => $cartss,
            'total' => Total::total(),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cart $cart)
    {
        $pId =  url()->current();
        $pId = collect(mb_substr($pId, -1));
        $delete = $cart
            ->where('product_id', '==', $pId)
            ->where('user_id', '==', Auth::user()->id)
            ->first();
        $cart->delete($delete);
        return response()->json([
            'cart_count' => Cart::count(),
        ]);
    }
}
