<?php

namespace App\Providers;

use App\Models\Cart;
use App\Models\Type;
use App\Models\User;
use App\Models\Brand;
use App\Models\Total;
use App\Models\Address;
use App\Models\Product;
use App\Models\Delivery;
use App\Models\ProductColor;

use App\Models\PaymentMethod;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        Gate::define('admin', function (User $user) {
            return $user->role === 'Admin';
        });
        Gate::define('user', function (User $user) {
            return $user->role === 'User';
        });


        view()->composer('*', function ($view) {
            $view->with([
                // 'product' => @Product::with('detail.color','type', 'brand')->get(),
                'carts' => @Cart::where('user_id', '=', Auth::user()->id)->get(),
                'total' => @Total::total(),
                'types' => @Type::with('product')->get(),
                'brands' => @Brand::with('product')->get(),
                'deliveries' => Delivery::all(),
                'payments' => PaymentMethod::all(),
                'colors' => ProductColor::all(),
                // 'addresses' => Address::where('user_id', Auth::user()->id)->get(),
            ]);
        });
    }
}
