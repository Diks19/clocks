<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//WEB
Route::get('/', function () {
    return redirect(route('product.index'));
});
Illuminate\Support\Facades\Auth::routes();
Route::middleware('auth')->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});
Route::middleware('user')->group(function () {
    Route::resource('cart', App\Http\Controllers\CartController::class)->except('index', 'show', 'create', 'edit');
    Route::resource('transaction', App\Http\Controllers\TransactionController::class)->only('index', 'store');
});
Route::get('product/type/{type}', [App\Http\Controllers\SubProductController::class, 'productType'])->name('product.type');
Route::get('product/brands/{brand}', [App\Http\Controllers\SubProductController::class, 'productBrand'])->name('product.brand');
// Route::get('product/color/{productColor}', [App\Http\Controllers\SubProductController::class, 'productColor'])->name('product.color');
Route::resource('product', App\Http\Controllers\ProductController::class);
Route::middleware('admin')->group(function () {
    Route::get('brand', [App\Http\Controllers\BrandController::class, 'index']);
    Route::get('payment', [App\Http\Controllers\PaymentMethodController::class, 'index']);
    Route::get('delivery', [App\Http\Controllers\DeliveryController::class, 'index']);
    Route::get('type', [App\Http\Controllers\TypeController::class, 'index']);
    Route::get('color', [App\Http\Controllers\ProductColorController::class, 'index']);
});




//API
Route::middleware('auth')->group(function () {
});
Route::middleware('user')->group(function () {
    Route::apiResource('api/address', \App\Http\Controllers\Api\AddressController::class);
    Route::get('api/province', [\App\Http\Controllers\Api\RajaOngkir::class, 'province']);
    Route::get('api/city/{province}', [\App\Http\Controllers\Api\RajaOngkir::class, 'city']);
    Route::post('api/cost', [\App\Http\Controllers\Api\RajaOngkir::class, 'cost'])->name('raja.cost');
});
Route::middleware('admin')->group(function () {
    Route::apiResource('api/type', \App\Http\Controllers\Api\TypeController::class);
    Route::apiResource('api/brand', \App\Http\Controllers\Api\BrandController::class);
    Route::apiResource('api/payment', \App\Http\Controllers\Api\PaymentController::class);
    Route::apiResource('api/delivery', \App\Http\Controllers\Api\DeliveryController::class);
    Route::apiResource('api/color', \App\Http\Controllers\Api\ProductColorController::class);
});
