<?php

use App\Models\Cart;
use App\Models\Total;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
});
Route::get('cart/{cart}', function (Cart $cart) {
    $carts['price_total'] = $cart->price_total;
    return $carts;
});
Route::get('/total/{id}', function ($id) {
    $total['total'] = Total::totalApi($id);
    return $total;
});
