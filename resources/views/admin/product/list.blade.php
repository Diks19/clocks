@extends('admin.layouts.app')

@section('title', 'Product')
@section('product', 'active')

@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Product Data</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                   {{-- <li class="breadcrumb-item"><a href="{{route('jenis.index')}}">jenis</a>
                                   </li> --}}
                                <li class="breadcrumb-item active">List
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div class="row" id="basic-table">
                @can('admin')
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <a href="{{route('product.create')}}" class="btn btn-primary waves-effect waves-float waves-light">Add Product</a>
                            </div>
                        </div>
                    </div>
                @endcan
            </div>
            <div class="d-flex justify-content-start">
                {{ $products->links() }}
            </div>
            <section id="card-demo-example">
                <div class="row match-height">
                    @foreach($products as $product)
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <a href="{{route('product.show',$product)}}"><img class="card-img-top mb-2" src="{{ asset ('storage/' . $product->detail->product_image)}}" alt="Product Image"></a>
                                    <a href="{{route('product.show',$product)}}" class="h1"><h1 class="card-title font-weight-bolder">{{$product->product_name}}</h1></a>
                                    <p class="card-text">
                                        Price : Rp. {{number_format($product->detail->price,2)}}
                                    </p>
                                    <p class="card-text font-weight-bolder">Product Name : {{ $product->product_name }}</p>
                                    <p class="card-text font-weight-bolder">Product Type : {{ $product->type->type }}</p>
                                    <p class="card-text font-weight-bolder">Product Brand : {{ $product->brand->brand_name }}</p>
                                    <div class="d-flex">
                                        <div class="justify-content-end">
                                            <p class="card-text font-weight-bolder item-center">Product Color : 
                                                @foreach ($product->detail->color as $color) 
                                                    <abbr class="ml-1" title="{{ $color->color_name }}"><a class="box-color" style="background-color: {{ $color->color_hex }};"></a></abbr>
                                                @endforeach
                                            </p>
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        @can('admin')
                                        <a href="{{route('product.edit',$product)}}" class="btn btn-warning">Edit</a>
                                        <a type="button" data-id="{{ $product->id }}" class="btn del-btn btn-danger">Delete</a>
                                        @endcan
                                    </div>
                                    {{-- <a href="{{route('produk.show',$product->id)}}" class="btn btn-primary">View More</a> --}}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
            </section>
        </div>
    </div>
</div>
@endsection

@push('styles')
<style>
    a.box-color{
        border-radius: 5px;
        padding: 2px;
        width: 20px;
        height: 20px;
        display: inline-block;
    }
</style>
@endpush

@push('script')

@if (session('success'))
{{-- <script>
    Swal.fire('Success','{{ session('success') }}','success')
</script> --}}
     <script>
         setTimeout(function () {
            toastr.success(
            '{{ session('success') }}',
            'Success!',
            {
                closeButton: true,
                tapToDismiss: true
            }
            );
        }, 1000);
     </script>
@endif

{{-- @if (session('success')) --}}
{{-- <script>
    Swal.fire('Success','{{ session('success') }}','success')
</script> --}}
     {{-- <script>
             setTimeout(function () {
                toastr.success(
                '{{ session('success') }}',
                'Success!',
                {
                    closeButton: true,
                    tapToDismiss: true
                }
                );
            }, 1000); 
     </script>
@endif --}}

@endpush
@push('scripts')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.del-btn', function () {
                var id = $(this).data('id');
                const form = document.querySelector('.form');
                Swal.fire({
                    icon: 'error',
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonColor: '#28C76F',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                })
                    .then((result) => {
                        if (result.value) {
                            $.ajax({
                                'url': '{{url('product')}}/' + id,
                                'type': 'post',
                                'data': {
                                    '_method': 'DELETE',
                                    '_token': '{{csrf_token()}}'
                                },
                                success: function (response) {
                                    if (response == 1) {
                                        toastr.error('Data Gagal dihapus!', 'Gagal!', {
                                            closeButton: true,
                                            tapToDismiss: false
                                        });
                                        // location.reload();
                                    } else {
                                        toastr.success('Data Berhasil dihapus!', 'Berhasil!', {
                                            closeButton: true,
                                            tapToDismiss: false
                                        });
                                        // setTimeout(() => {
                                        //     location.reload();
                                        // }, 1500);
                                    }
                                }
                            });
                        } else {
                            console.log(`dialog was dismissed by ${result.dismiss}`)
                        }

                    });
            });

        });

    </script>
@endpush



