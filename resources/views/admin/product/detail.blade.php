@extends('admin.layouts.app')
@section('product','active')
@section('title', 'Detail Product')
@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Data Product</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('product.index')}}">List</a>
                                    </li>
                                    <li class="breadcrumb-item active">{{(@$product ? ' Detail' : '')}} Product
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- app e-commerce details start -->
                <section id="basic-vertical-layouts">
                    <div class="row">
                        <div class=" col-12">
                            <div class="card">
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" role="alert">
                                            <h4 class="alert-heading">Error!</h4>
                                            <div class="alert-body">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @endif
                    <div class="row my-2">
                        <div class="col-12 col-md-5 d-flex align-items-center justify-content-center mb-2 mb-md-0">
                            <div class="d-flex align-items-center justify-content-center">
                                <img src="{{ asset('storage/' . $product->detail->product_image) }}" style="width: 22rem" class="img-fluid"/>
                            </div>
                        </div>
                        <div class="col-12 col-md-7">
                            <h1 class="font-weight-bolder">{{$product->product_name}}</h1>
                            <div class="ecommerce-details-price d-flex flex-wrap mt-1">
                                <h4 class="item-price mr-1">Rp. {{number_format($product->detail->price,2)}}</h4>
                                <ul class="unstyled-list list-inline pl-1 border-left">
                                    <li>⭐</li>
                                    <li>⭐</li>
                                    <li>⭐</li>
                                    <li>⭐</li>
                                    {{-- <li>⭐</li> --}}
                                </ul>
                            </div><hr>
                            <p><b>Type :</b></p>
                            <p class="card-text" >{{@$product->type->type}}</p><hr>
                            <p><b>Color : </b></p>
                            @foreach ($product->detail->color as $color)
                                <abbr title="{{ $color->color_name }}">
                                        <a class="box-color" style="background-color: {{ $color->color_hex }};"></a>
                                </abbr>
                            @endforeach
                            <hr>
                            <p><b>Description :</b></p>                                
                            <p class="card-text">
                                {{@$product->detail->description}}
                            </p>
                            <hr />
                                <h6 class="font-weight-bolder">Available Stock : </h6>
                                <p>
                                    {{@$product->detail->stock}}
                                </p>
                            <hr />
                            
                            <div class="d-flex flex-column flex-sm-row pt-1">
                                @can('admin')
                                    <a href="{{route('product.edit',$product)}}" class="btn btn-warning">Edit</a>
                                    <form action="{{ route('product.destroy', $product) }}" class="d-inline" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn del-btn btn-danger ml-1">Delete</button>
                                    </form>
                                @endcan
                                @can('user')
                                    <button type="button" class="btn btn-primary btn-cart mr-0 mr-sm-1 mb-1 mb-sm-0 ml-2" data-toggle="modal" data-target="#show">
                                        <i data-feather="shopping-cart" class="mr-50"></i>
                                        <span class="add-to-cart">Add to cart</span>
                                    </button>
                                @endcan
                                <!-- Modal -->
                                <div class="modal" id="show" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-lg">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Select Color</h5>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{ route('cart.store') }}" class="color" method="post">
                                            @csrf
                                            @php
                                                $url =  $_SERVER["REQUEST_URI"];
                                                $url = collect(str_split($url));
                                                $url = $url->splice(9)->implode('');
                                            @endphp

                                            <div class="col-12">
                                                <div class="form-group">
                                                   <input type="hidden" id="pId" name="product_id" value="{{ $url }}">
                                                   <input type="hidden" id="uId" name="user_id" value="{{ @ Auth::user()->id }}">
                                                   
                                                   <label class="mr-1" for="qty">Quantity : </label>
                                                    <button class="btn btn-outline-dark flex-shrink-0 form-control" type="button" id="minus" style="width: 55px ">
                                                        <span class="font-weight-bolder">-</span>
                                                    </button>
                                                    <input class="form-control d-inline form-control" id="qty" type="number" name="qty" value="0" style="width: 55px" />
                                                    <button class="btn btn-outline-dark flex-shrink-0 form-control" type="button" id="plus" style="width: 55px">
                                                        <span class="font-weight-bolder">+</span>
                                                    </button> 
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="color">Select Color : </label>
                                                    <select name="color_id" id="cId" class="form-select form-control">
                                                @foreach ($product->detail->color as $color)
                                                    <option value="{{ $color->id }}">{{ $color->color_name }}</option>
                                                @endforeach
                                            </select>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-end">
                                                Total : Rp. <p class="disabled pricee d-inline-block">0</p>
                                                <input type="hidden" id="priceee" name="price_total">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="modal">Close</button>
                                        <button type="button" class="btn btn-primary" id="select">Select</button>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>                                   
                                
@endsection

@push('styles')
<style>
    a.box-color{
        border-radius: 25px;
        padding: 2px;
        width: 20px;
        height: 20px;
        display: inline-block;
    }
</style>
@endpush

@push('scripts')
<script>
$(document).on('keypress', (e)=>{
    if (e.witch == '13') {
        e.preventDefault();
    }
})
const minusButton = document.getElementById('minus');
const plusButton = document.getElementById('plus');
const inputField = document.getElementById('qty');
const input = document.getElementById('priceee')
var max = '{{ $product->detail->stock }}';

$(inputField).on('keyup', () => {
    const currentValue = Number(inputField.value);
    if (currentValue != 0 && currentValue <= max){
        var price = $('.pricee').text(number_format('{{ $product->detail->price }}'* +inputField.value));
        input.value = price.text().replace(/,/g, '');
    }else{
        $('priceee').val('0')
        $('.pricee').text('0');
    }
})  
minusButton.addEventListener('click', event => {
    event.preventDefault();
    const currentValue = Number(inputField.value) || 0;
    if (currentValue != 0){
        inputField.value = currentValue - 1;
        var price = $('.pricee').text(number_format('{{ $product->detail->price }}'*+inputField.value));
        input.value = price.text().replace(/,/g, '');
    }else{
        $('.pricee').text('0');
    }
    
});

plusButton.addEventListener('click', event => {
    event.preventDefault();
    const currentValue = Number(inputField.value) || 0;
    if (currentValue < max) {
        inputField.value = currentValue + 1;
        var price = $('.pricee').text(number_format('{{ $product->detail->price }}'*+inputField.value));
        input.value = price.text().replace(/,/g, '');
    }

});
    $('button#select').on('click', function() {
            event.preventDefault();
            $('#modal').click();
            if(inputField.value < 1) {
                toastr['error']('', "Error!",{
                    closeButton: true,
                    tapToDismiss: true
                    });
            }else{
                $.ajax({
                    'url' : '{{ route('cart.store') }}', 
                    'type' : 'POST',
                    'data' : {
                        'product_id': $('#pId').val(),
                        'user_id': $('#uId').val(),
                        'qty': inputField.value,
                        'color_id': $('#cId').val(), 
                        'price_total': input.value,
                        '_token': '{{ csrf_token() }}',
                    },
                    success: function(data) {
                        toastr['success'](data.message, 'Success',{
                            closeButton: true,
                            tapToDismiss: true
                            });
                        var total = data.total;
                        var price = $('#total').text('Rp. ' + number_format(total));
                        let rows = '';
                        if (data.cart_count) {
                            rows += '<div class="media align-items-center">'+
                                        '<img class="d-block rounded mr-1" src="{{ asset('storage') }}/'+data.cart.product.detail.product_image+' " width="40">'+
                                            '<div class="media-body">'+
                                                '<div class="media-heading">'+
                                                    '<h6 class="cart-item-title .h6">'+
                                                        '<a class="text-body" href="app-ecommerce-details.html"> '+data.cart.product.product_name+' </a>'+
                                                    '</h6> '+
                                                    '<small class="cart-item-by"> '+data.cart.color.color_name+' </small> '+
                                                '</div>'+
                                                '<div class="cart-item-qty">'+
                                                    '<div class="input-group">'+
                                                        '<input class="touchspin-cart a" id="quantity" data-id=" '+data.cart.id+'" type="number" value="'+data.cart.qty+'">'+
                                                    '</div>'+
                                                '</div>'+
                                                '<h6 class="cart-item-price font-weight-1 price-item" id="price-item">Rp.  '+number_format(data.cart.price_total)+' </h6>'+
                                            '</div>'+
                                    '</div>';
                                    $('#total').html('Rp ' +number_format(total));
                            $('#for').append(rows);
                            $('#cart-itemss').text(data.cart_count + ' Items');
                            $('#bulet').text(data.cart_count);
                            

                        }
                        $('#empty-note').html('');
                        $('#empty-note').attr('class', '');
                        var rowsTotal = '';
                        if (data.cart_count == 1) {
                            rowsTotal += '<li id="total-note" class="dropdown-menu-footer" >'+
                                            '<div class="d-flex justify-content-between mb-1">'+
                                                '<h6 class="font-weight-bolder mb-0">Total: </h6>'+
                                                '<h6 class="text-primary font-weight-bolder mb-0" id="total">Rp.'+ number_format(total)+'</h6>'+
                                            '</div><a class="btn btn-primary btn-block" href="{{ route('transaction.index') }}">Checkout</a>'+
                                        '</li>';
                            $('#total-note').html(rowsTotal);
                        }
                        const quantity = document.querySelectorAll('#quantity');
                        $(quantity).TouchSpin({
                            'max': '{{@$product->detail->stock}}',
                        });
                    }, 
                    error: function(err){
                        var w = window. open('about:blank');
                            w. document. open();
                            w. document. write(err.responseText);
                            w. document. close();
                    }
                })
            }
        })


</script>
@endpush
