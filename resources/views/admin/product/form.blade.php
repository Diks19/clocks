@extends('admin.layouts.app')
@section('title')
    {{ @$product ? 'Update Product' : 'Add Product' }}
@endsection
@section('product', 'active')
@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Product Data</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('product.index')}}">List</a>
                                    </li>
                                    <li class="breadcrumb-item active">{{(@$product ? ' Update' : 'Add')}} Product
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic Tables start -->
                <section id="basic-vertical-layouts">
                    <div class="row">
                        <div class=" col-12">
                            <div class="card">
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" role="alert">
                                            <h4 class="alert-heading">Error!</h4>
                                            <div class="alert-body">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @endif
                                    <form class="form form-vertical" id="form"  action="{{@$product ? route('product.update', $product) : route('product.store')}}"
                                          method="POST" enctype="multipart/form-data">
                                        @csrf
                                        @if(@$product)
                                            @method('PUT')
                                        @endif
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="product_name">Product Name</label>
                                                    <input type="text" id="produk" class="form-control" name="product_name" placeholder="Product Name"
                                                           value="{{old('product_name', @$product ? $product->product_name : '')}}"/>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="type">Type</label>
                                                    <select class="form-select form-control" id="type" name="type_id" >
                                                        <option value="">- Select Product Type -</option>
                                                        @foreach($types as $type) 
                                                            <option value="{{$type->id}}" {{@$product && $product->type->id == $type->id ? 'selected' : '' }}>{{$type->type}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="brand">Brand</label>
                                                    <select class="form-select form-control" id="brand" name="brand_id" >
                                                        <option value="">- Choose Product Brand -</option>
                                                        @foreach($brands as $brand)
                                                            <option value="{{$brand->id}}" {{@$product && $product->brand->id == $brand->id ? 'selected' : '' }}>{{$brand->brand_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            @if (@$product)

                                            @else
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="color">Color </label>
                                                    <select class="form-select form-control select2" multiple="multiple" id="color" name="color[]" >
                                                        @foreach ($colors as $color)
                                                            <option value="{{$color->id}}">
                                                                {{$color->color_name}}
                                                            </option>
                                                        @endforeach
                                                    </select>    
                                                    <p class="disabled">can't be edited, make sure you enter the color correctly</p>                                                   
                                                </div>
                                            </div>
                                            @endif
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="stock">Stock</label>
                                                    <input type="number" min="0" id="stock" class="form-control" name="stock" placeholder="10" value="{{old('stock', @$product ? $product->detail->stock : '')}}">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="price">Price</label>
                                                    <input type="number" min="0" id="price" class="form-control" name="price" placeholder="100000"
                                                           value="{{old('price', @$product ? $product->detail->price : '')}}"/>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                    <div class="form-group">
                                                    <label for="description">Description</label>
                                                    <textarea type="text" class="form-control" name="description"> {{ @$product->detail->description }} </textarea> 
                                             </div>
                                                </div>
                                                
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="productDetails_image">Product Image</label>
                                                    @if (@$product)
                                                    <input type="hidden" name="old_product_image" value="{{ $product->detail->product_image }}">
                                                    @else
                                                    @endif
                                                    <input type="file" id="productDetails_image" class="form-control" name="product_image">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary mr-1">Submit</button>
                                                <a href="{{route('product.index')}}" type="reset" class="btn btn-outline-secondary">Cancel</a>
                                            </div>
                                        </div>
                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Basic Tables end -->
            </div>
        </div>
    </div>

@endsection

@push('styles')
@endpush

@push('scripts')
    <script>
        $('.select2').select2();
    </script>
@endpush


