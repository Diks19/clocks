@extends('admin.layouts.app')
@section('title', 'Payment Method')
@section('payment', 'active')

@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Data Payment Method</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
{{--                                    <li class="breadcrumb-item"><a href="{{route('brand.index')}}">brand</a>--}}
{{--                                    </li>--}}
                                    <li class="breadcrumb-item active">List
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic Tables start -->
                <div class="row" id="basic-table">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <button id="add-btn" class="btn btn-primary waves-effect waves-float waves-light" data-toggle="modal" data-target="#form-modal-add">
                                    Add Payment Method
                                </button>
                            </div>
                            <!-- Modal Add-->
                            <div class="modal fade" data-backdrop="static" id="form-modal-add" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-md" role="document">
                                    <div class="modal-content">
                                            <div class="modal-header">
                                                    <h5 class="modal-title">Form Create</h5>
                                                </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                                <div class="form-group">
                                                  <label for="in-pay">Payment Method Name</label>
                                                  <input type="text" class="form-control" id="in-pay">
                                                  <small class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="btn-close-add" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" id="btn-save-add" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal Update-->
                            <div class="modal fade" data-backdrop="static" id="form-modal-edit" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-dialog-md" role="document">
                                    <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Form Edit</h5>
                                            </div>
                                        <div class="modal-body">
                                            <div class="container-fluid">
                                                <div class="form-group">
                                                  <label for="ed-pay">Payment method Name</label>
                                                  <input type="text" class="form-control" id="ed-pay">
                                                  <small class="form-text text-danger"></small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="btn-close-edit" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" id="btn-save-edit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Payment Method</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="list">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Tables end -->
            </div>
        </div>
    </div>

@endsection

@push('styles')
@endpush

@push('scripts')
    <script>
        $(document).ready(function () {
           getPay();
            
            $('#btn-save-add').on('click', function(){
                $.ajax({
                    type: "POST",
                    url: "{{ url('api/payment') }}",
                    data: {
                        'payment_method': $('#in-pay').val(),
                        '_token': '{{ csrf_token() }}'
                    },
                    success: function (response) {
                        if (response.code === 200) {
                            $('#in-pay').val('');
                        }
                        $(document).find('#form-modal-add').find('#btn-close-add').click();
                        getPay();
                        if(response.code == 200){
                            return toastr.success(response.message, 'Success!', {
                                closeButton: true,
                                tapToDismiss: true
                            });
                        }
                        $.each(response.error, function (idx, err) { 
                                toastr.error(err, 'Error!', {
                                closeButton: true,
                                tapToDismiss: true
                            });
                        });
                    }
                });
            });
            $(document).on('click', '#update-btn', function() {
                setTimeout(() => {
                    $('#modal-header').text('Form Edit');
                }, 100);
                const thisIs = $(this);
                const id = $(this).data('id');
                $.ajax({
                    type: "GET",
                    url: "{{ url('api/payment') }}/"+id,
                    dataType: "JSON",
                    success: function (response) {
                        $(document).on('click', '#btn-close-edit', function(){
                            id = null;
                        });
                        $('#ed-pay').val(response.data.payment_method);
                        $(thisIs).parents(document).find('#btn-save-edit').on('click', function(){
                            $.ajax({
                                type: "POST",
                                url: "{{ url('api/payment') }}/"+id,
                                data: {
                                    'payment_method': $('#ed-pay').val(),
                                    '_method': 'PUT',
                                    '_token': '{{ csrf_token() }}'
                                },
                                success: function (response) {
                                    $(thisIs).parents(document).find('#form-modal-edit').find('#btn-close-edit').click();
                                    getPay();
                                    if(response.code === 200){
                                        return toastr.success(response.message, 'Success!', {
                                            closeButton: true,
                                            tapToDismiss: true
                                        });
                                    }
                                    $.each(response.error, function (idx, err) { 
                                        toastr.error(err, 'Error!', {
                                            closeButton: true,
                                            tapToDismiss: true
                                        });
                                    });
                                }
                            });
                        });
                    }
                });
            });
            $(document).on('click', '#del-btn', function () {
                var id = $(this).data('id');
                Swal.fire({
                    icon: 'error',
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonColor: '#28C76F',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                })
                .then((result) => {
                    if (result.value) {
                        $.ajax({
                            'url': '{{url('api/payment')}}/' + id,
                            'type': 'POST',
                            'data': {
                                '_method': 'DELETE',
                                '_token': '{{csrf_token()}}'
                            },
                            success: function (response) {
                                if (response.message) {
                                    getPay();
                                    return toastr.success(response.message, 'Success!', {
                                        closeButton: true,
                                        tapToDismiss: true
                                    });
                                }
                                    getPay();
                                    return toastr.error('Failed!', 'Failed!', {
                                        closeButton: true,
                                        tapToDismiss: true
                                    });
                            }
                        });
                    } else {
                        console.log(`dialog was dismissed by ${result.dismiss}`)
                    }
                });
            });
            function getPay() {
                $.ajax({
                    type: "GET",
                    url: "{{ url('api/payment') }}",
                    dataType: "JSON",
                    success: function (response) {
                        let rows = '';
                        $.each(response.datas, function (idx, data) { 
                            idx++
                            rows += '<tr>'+
                                        '<td>'+idx+'</td>'+
                                        '<td>'+data.payment_method+'</td>'+
                                        '<td>'+
                                            '<div class="dropdown">'+
                                                '<button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">'+
                                                    '<i data-feather="more-vertical"></i>'+
                                                '</button>'+
                                                '<div class="dropdown-menu">'+
                                                    '<a class="dropdown-item" id="update-btn" href="#" data-id="'+data.id+'" data-toggle="modal" data-target="#form-modal-edit">'+
                                                        '<i data-feather="edit-2" class="mr-50"></i>'+
                                                        '<span>Edit</span>'+
                                                    '</a>'+
                                                    '<a class="dropdown-item" id="del-btn" href="#" data-id="'+data.id+'">'+
                                                        '<i data-feather="trash" class="mr-50"></i>'+
                                                        '<span>Delete</span>'+
                                                    '</a>'+
                                                '</div>'+
                                            '</div>'+
                                        '</td>'+
                                    '</tr>';  
                        });
                        $('#list').html('');
                        $('#list').append(rows);
                        if (feather) {
                            feather.replace({
                                width: 14,
                                height: 14
                            });
                        }
                    }
                });
            }
        });

    </script>
@endpush


