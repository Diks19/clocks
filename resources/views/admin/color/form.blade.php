@extends('admin.layouts.app')
@section('title')
    {{ @$color ? 'Update' : 'Add' }} Color
@endsection
@section('color', 'active')
@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Color Data</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('color.index')}}">List</a>
                                    </li>
                                    <li class="breadcrumb-item active">{{(@$color ? 'Update' : 'Add')}} Color

                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic Tables start -->
                <section id="basic-vertical-layouts">
                    <div class="row">
                        <div class=" col-12">
                            <div class="card">
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" role="alert">
                                            <h4 class="alert-heading">Error!</h4>
                                            <div class="alert-body">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @endif
                                    <form class="form form-vertical"  action="{{@$color ? route('color.update',$color) : route('color.store')}}"
                                          method="POST" enccolor="multipart/form-data">
                                        @csrf
                                        @if(@$color)
                                          @method('PUT')
                                        @endif
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="color">Color Name</label>
                                                    <input color="text" id="color" class="form-control" name="color_name" value="{{old('color_name', @$color ? $color->color_name : '')}}"/>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label for="color">Color Hex</label>
                                                    <input id="clr" class="form-control" type="color" name="color_hex" value="{{ old('color_hex', @$color ? @$color->color_hex : '') }}">
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <button color="submit" id="save" class="btn btn-primary mr-1">Submit</button>
                                                <a href="{{route('color.index')}}" color="reset" class="btn btn-outline-secondary">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Basic Tables end -->
            </div>
        </div>
    </div>

@endsection

@push('styles')
@endpush

@push('scripts')
    <script>
        $('#save').on('click', function(){
            event.preventDefault();
            console.log($('#clr').val(  ));
        })
    </script>
@endpush
