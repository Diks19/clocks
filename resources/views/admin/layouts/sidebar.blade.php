<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ asset('assets') }}/admin/html/ltr/vertical-menu-template/index.html"><span class="brand-logo">
                            <svg viewbox="0 0 139 95" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="24">
                                <defs>
                                    <lineargradient id="linearGradient-1" x1="100%" y1="10.5120544%" x2="50%" y2="89.4879456%">
                                        <stop stop-color="#000000" offset="0%"></stop>
                                        <stop stop-color="#FFFFFF" offset="100%"></stop>
                                    </lineargradient>
                                    <lineargradient id="linearGradient-2" x1="64.0437835%" y1="46.3276743%" x2="37.373316%" y2="100%">
                                        <stop stop-color="#EEEEEE" stop-opacity="0" offset="0%"></stop>
                                        <stop stop-color="#FFFFFF" offset="100%"></stop>
                                    </lineargradient>
                                </defs>
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="Artboard" transform="translate(-400.000000, -178.000000)">
                                        <g id="Group" transform="translate(400.000000, 178.000000)">
                                            <path class="text-primary" id="Path" d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z" style="fill:currentColor"></path>
                                            <path id="Path1" d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z" fill="url(#linearGradient-1)" opacity="0.2"></path>
                                            <polygon id="Path-2" fill="#000000" opacity="0.049999997" points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325"></polygon>
                                            <polygon id="Path-21" fill="#000000" opacity="0.099999994" points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338"></polygon>
                                            <polygon id="Path-3" fill="url(#linearGradient-2)" opacity="0.099999994" points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"></polygon>
                                        </g>
                                    </g>
                                </g>
                            </svg></span>
                        <h2 class="brand-text">Clock</h2>
                    </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                @auth
                    <li class="@yield('home') nav-item"><a class="d-flex align-items-center" href="{{ route('home') }}"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Dashboards">Dashboards</span></a>
                    </li>
                @endauth
                <li class=" navigation-header"><span>Product</span><i data-feather="more-horizontal"></i>
                </li>
                <li class="nav-item has-sub">
                    <a class="d-flex align-items-center" href="#"><i data-feather="box"></i><span class="menu-title text-truncate" data-i18n="product">Product</span>
                    </a>
                    <ul class="menu-content">
                        <li class="@yield('product')">
                            <a class="d-flex align-items-center" href="{{ route('product.index') }}"><i data-feather="box"></i><span class="menu-title text-truncate" data-i18n="all-product">All Product</span>
                            </a>
                        </li>
                        <li class="nav-item has-sub">
                            <a class="d-flex align-items-center" href="#"><i data-feather="box"></i><span class="menu-title text-truncate" data-i18n="product">Product By Type</span></a>
                            <ul class="menu-content">
                                @foreach ($types as $type)
                                @php
                                    $urlType =  $_SERVER["REQUEST_URI"];
                                    $urlType = collect(str_split($urlType));
                                    $urlType = $urlType->splice(14)->implode('');
                                @endphp
                                <li class="{{ $type->id == $urlType ? 'active' : ''}}">
                                    <a class="d-flex align-items-center" href="{{ route('product.type', $type) }}"><i data-feather="box"></i><span class="menu-title text-truncate" data-i18n="product-type">{{ $type->type }}</span>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="nav-item has-sub">
                            <a class="d-flex align-items-center" href="#"><i data-feather="box"></i><span class="menu-title text-truncate" data-i18n="product">Product By Brand</span></a>
                            <ul class="menu-content">
                                @foreach ($brands as $brand)
                                @php
                                    $urlBrand =  $_SERVER["REQUEST_URI"];
                                    $urlBrand = collect(str_split($urlBrand));
                                    $urlBrand = $urlBrand->splice(16)->implode('');
                                    $nav = 'brand'.$loop->iteration;
                                    $urlBrand = 'brand'.$urlBrand;
                                @endphp
                                <li class="{{ $nav == $urlBrand ? 'active' : '' }}">
                                    <a class="d-flex align-items-center" href="{{ route('product.brand', $brand) }}"><i data-feather="box"></i><span class="menu-title text-truncate" data-i18n="product-brand">{{ $brand->brand_name }}</span>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        {{-- <li class="nav-item has-sub">
                            <a class="d-flex align-items-center" href="#"><i data-feather="box"></i><span class="menu-title text-truncate" data-i18n="product">Product By Color</span></a>
                            <ul class="menu-content">
                                @foreach ($colors as $color)
                                @php
                                    $urlColor =  $_SERVER["REQUEST_URI"];
                                    $urlColor = collect(str_split($urlColor));
                                    $urlColor = $urlColor->splice(15)->implode('');
                                    $nav = 'color'.$loop->iteration;
                                    $urlColor = 'color'.$urlColor;
                                @endphp
                                <li class="{{ $nav == $urlColor ? 'active' : '' }}">
                                    <a class="d-flex align-items-center" href="{{ route('product.color', $color) }}"><i data-feather="box"></i><span class="menu-title text-truncate" data-i18n="product-brand">{{ $color->color_name }}</span>
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        </li> --}}
                    </ul>
                </li>
                @can('user')
                    <li class=" navigation-header"><span>Transaction</span><i data-feather="more-horizontal"></i>
                    </li>
                    <li class="@yield('transaction') nav-item">
                        <a class="d-flex align-items-center" href="{{ route('transaction.index') }}"><i data-feather="dollar-sign"></i><span class="menu-title text-truncate" data-i18n="product">Transaction</span>
                        </a>
                    </li>
                @endcan
                </li>
                @can('admin')
                <li class="@yield('brand') nav-item"><a class="d-flex align-items-center" href="{{ url('brand') }}"><i data-feather="tag"></i><span class="menu-title text-truncate" data-i18n="brand">Brand</span></a>
                </li>
                <li class="@yield('type') nav-item"><a class="d-flex align-items-center" href="{{ url('type') }}"><i data-feather="type"></i><span class="menu-title text-truncate" data-i18n="type">Type</span></a>
                </li>
                <li class="@yield('color') nav-item"><a class="d-flex align-items-center" href="{{ url('color') }}"><i data-feather="droplet"></i><span class="menu-title text-truncate" data-i18n="type">Color</span></a>
                </li>
                <li class="@yield('delivery') nav-item"><a class="d-flex align-items-center" href="{{ url('delivery') }}"><i data-feather="package"></i><span class="menu-title text-truncate" data-i18n="type">Delivery</span></a>
                </li>
                <li class="@yield('payment') nav-item"><a class="d-flex align-items-center" href="{{ url('payment') }}"><i data-feather="dollar-sign"></i><span class="menu-title text-truncate" data-i18n="type">Payment Method</span></a>
                </li>
                {{-- <li class="@yield('stock') nav-item"><a class="d-flex align-items-center" href="{{ route('stock.index') }}"><i data-feather="droplet"></i><span class="menu-title text-truncate" data-i18n="type">Product Stock</span></a>
                </li> --}}
                @endcan
                
                
        </div>
</div>



