<!-- BEGIN: Vendor JS-->
    <script src="{{ asset('assets') }}/admin/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('assets') }}/admin/vendors/js/charts/apexcharts.min.js"></script>
    <script src="{{ asset('assets') }}/admin/vendors/js/extensions/toastr.min.js"></script>
    <script src="{{ asset('assets') }}/admin/vendors/js/extensions/sweetalert2.all.min.js"></script>
    
    <script src="{{ asset('assets') }}/admin/vendors/js/forms/wizard/bs-stepper.min.js"></script>
    <script src="{{ asset('assets') }}/admin/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('assets') }}/admin/js/core/app-menu.js"></script>
    <script src="{{ asset('assets') }}/admin/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    @stack('pagejs')
    {{-- <script src="{{ asset('assets') }}/admin/js/scripts/pages/dashboard-ecommerce.js"></script> --}}
    <script src="{{ asset('assets') }}/admin/js/select2.full.min.js"></script>
    <!-- END: Page JS-->
    <script src="{{ asset('js/myFunc.js') }}"></script>
    <script>
    $(document).ready(function () {
        // UPDATE BUTTON
        $(document).on('keyup', '#quantity', function () {
            var id = $(this).data('id');
            var pId = $(this).data('product');
            var qty = $(this).val();
            if(qty >= 0){
                $.ajax({
                    'url': '{{url('cart')}}/' + id,
                    'type': 'POST',
                    'data': {
                        'id': id,
                        'qty': qty,
                        'type': 3,
                        '_method': 'PUT',
                        '_token': '{{csrf_token()}}',
                    },
                    success: function (response) {
                        if (response == 1) {
                            toastr.error('Data Gagal ditambah!', 'Gagal!', {
                                closeButton: true,
                                tapToDismiss: true
                            });
                        } else {
                        }
                    }
                });
            } else{ 
                console.log('gagal');
            }
        });
        const inQuantity = document.querySelectorAll('#quantity');
        inQuantity.forEach(qty => {
            $(qty).on('change', function () {
                $(qty).TouchSpin({
                    'max': 100,
                    'min': 1,
                })
                if ($(qty).val() == 0) {
                    $(qty).val(1);
                }
            });
        });
        const delBtn = document.querySelectorAll('#btn-del-cart');
        delBtn.forEach(del => {
            $(del).click(function () {
                var id = $(this).data('id');
                Swal.fire({
                    icon: 'error',
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonColor: '#28C76F',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                })
                .then((result) => {
                    if (result.value) {
                        $(this).parents('.media.align-items-center').remove();
                        $.ajax({
                            'url': '{{url('cart')}}/' + id,
                            'type': 'POST',
                            'data': {
                                '_method': 'DELETE',
                                '_token': '{{csrf_token()}}',
                            },
                            success: function (data) {
                                toastr.success('Data Berhasi dihapus', 'Berhasil!', {
                                    closeButton: true,
                                    tapToDismiss: true
                                });
                                if (data.cart_count == null) {
                                    data.cart_count = 0;
                                }
                                $('#cart-itemss').text(data.cart_count + ' Items');
                                $('#bulet').text(data.cart_count);
                                var rowsEmpty = '';
                                if (data.cart_count == 0) {
                                    $('#total-note').attr('class', '');
                                    $('#total-note').html('');
                                    rowsEmpty += '<div id="empty-note" class="media align-items-center">'+
                                                    '<div class="media-body d-flex justify-content-center">'+
                                                        '<p class="text-danger">Cart is empty</p>'+
                                                    '</div>'+
                                                '</div>';
                                }
                                $('#empty-note').html(rowsEmpty);
                            }
                        });
                    } else {
                        toastr['error']('Failed add to cart!', 'Failed',{
                            closeButton: true,
                            tapToDismiss: true
                            });
                    }
                });
            }); 
        });
        
        // UPDATE INPUT
        $(document).on('change', '#quantity', function () {
            var id = $(this).data('id');
            var qty = $(this).val();
            var thisIs = $(this);
            $.ajax({
                'url': '{{url('cart')}}/' + id,
                'type': 'POST',
                'data': {
                    'id': id,
                    'qty': qty,
                    '_method': 'PUT',
                    '_token': '{{csrf_token()}}',
                },
                success: function (data) {
                        $('#total').text('Rp. ' + number_format(data.total));
                        var prices = thisIs.parents('div.media-body');
                        prices.find('#price-item').text('Rp. ' + number_format(data.cart.price_total));
                }
            });
        });
    });
    </script>
    @stack('script')
    @stack('scripts')
    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>