@extends('admin.layouts.app')
@section('title', 'Transaction ')
@section('transaction', 'active ')
@section('content')
    <div class="app-content content ecommerce-application">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Checkout</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="#">eCommerce</a>
                                    </li>
                                    <li class="breadcrumb-item active">Checkout
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <div class="bs-stepper checkout-tab-steps">
                    <!-- Wizard starts -->
                    <div class="bs-stepper-header">
                        <div class="step" data-target="#step-cart">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="shopping-cart" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Cart</span>
                                    <span class="bs-stepper-subtitle">Your Cart Items</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#step-address">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="home" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Address</span>
                                    <span class="bs-stepper-subtitle">Enter Your Address</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#step-payment">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">
                                    <i data-feather="credit-card" class="font-medium-3"></i>
                                </span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Payment</span>
                                    <span class="bs-stepper-subtitle">Select Payment Method</span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <!-- Wizard ends -->

                    <div class="bs-stepper-content">
                        <!-- Checkout Place order starts -->
                        <div id="step-cart" class="content">
                            <div id="place-order" class="list-view product-checkout">
                                <!-- Checkout Place Order Left starts -->
                                <div class="checkout-items">
                                    @foreach ($carts as $cart)
                                    <div class="card ecommerce-card">
                                        <div class="item-img">
                                            <a href="app-ecommerce-details.html">
                                                <img src="{{ asset('assets') }}/admin/images/pages/eCommerce/1.png" alt="img-placeholder" />
                                            </a>
                                        </div>
                                        <div class="card-body">
                                            <div class="item-name">
                                                <h6 class="mb-0"><a href="app-ecommerce-details.html" class="text-body">{{ $cart->product->product_name }}</a></h6>
                                                <div class="item-rating">
                                                    <ul class="unstyled-list list-inline">
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            @if ($cart->product->detail->stock == 0)
                                            <span class="text-danger ">Not In Stock</span>
                                            @else
                                            <span class="text-success ">In Stock</span>
                                            @endif
                                            <span class="text-muted mb-1">{{ $cart->color->color_name }}</span>
                                            <div class="item-quantity">
                                                <span class="quantity-title">Qty:</span>
                                                <div class="input-group quantity-counter-wrapper">
                                                    <input type="text" class="quantity-counter" value="{{ $cart->qty }}" data-id="{{ $cart->id }}" id="quantitycheckout"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-options text-center">
                                            <div class="item-wrapper">
                                                <div class="item-cost">
                                                    <h4 class="item-price">Rp. {{ number_format($cart->price_total) }}</h4>
                                                    <p class="card-text shipping">
                                                        <span class="badge badge-pill badge-light-success">Free Shipping</span>
                                                    </p>
                                                </div>
                                            </div>
                                            <button type="button" data-id="{{ $cart->id }}" class="btn btn-light mt-1 remove-wishlist">
                                                <i data-feather="x" class="align-middle mr-25"></i>
                                                <span>Remove</span>
                                            </button>
                                        </div>
                                    </div>
                                    @endforeach
                                    
                                </div>
                                <!-- Checkout Place Order Left ends -->

                                <!-- Checkout Place Order Right starts -->
                                <div class="checkout-options">
                                    <div class="card">
                                        <div class="card-body">
                                            <label class="section-label">Options</label>
                                            <hr />
                                            <div class="price-details">
                                                <ul class="list-unstyled">
                                                    <li class="price-detail">
                                                        <div class="detail-title detail-total">Total</div>
                                                        <div id="total-co" class="detail-amt font-weight-bolder">Rp. {{ number_format($total) }}</div>
                                                    </li>
                                                </ul>
                                                <button type="button" class="btn btn-primary btn-block btn-next place-order">Place Order</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Checkout Place Order Right ends -->
                                </div>
                            </div>
                            <!-- Checkout Place order Ends -->
                        </div>
                        <!-- Checkout Customer Address Starts -->
                        <div id="step-address" class="content">
                            <form id="checkout-address" class="list-view product-checkout">
                                <div class="card">
                                    <div class="card-header flex-column align-items-start">
                                        <h4 class="card-title">Add New Address</h4>
                                        <p class="card-text text-muted mt-25">Be sure to check "Deliver to this address" when you have finished</p>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2">
                                                    <label for="checkout-name">Full Name:</label>
                                                    <input type="text" id="name" class="form-control" placeholder="John Doe" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2">
                                                    <label for="checkout-number">Mobile Number:</label>
                                                    <input type="number" id="number" class="form-control" placeholder="+62 85xxxxxxxxx" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2">
                                                    <label for="checkout-province">Province:</label>
                                                      <label for="Province"></label>
                                                      <select class="form-control" name="province" id="province">
                                                        <option value="default">Select Your Province</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2">
                                                    <label for="checkout-city">City:</label>
                                                    <select class="form-control" name="city" id="city">
                                                        <option value="default">Select Your City</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2">
                                                    <label for="checkout-pincode">Postal Code:</label>
                                                    <input type="number" id="postal-code" class="form-control" disabled />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2">
                                                    <label for="add-type">Address Type:</label>
                                                    <select class="form-control" id="add-type">
                                                        @foreach ($atypes as $type)
                                                        <option value="{{ $type->id }}">{{ $type->address_types }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group mb-2 mt-2">
                                                    <button style="margin-top: 1%;" id="save" type="button" class="btn btn-primary">Save</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <div id="list-address">

                                </div>
                            </form>
                        </div>
                        <!-- Checkout Customer Address Ends -->

                        <!-- Checkout Payment Starts -->
                        <div id="step-payment" class="content">
                            <form id="checkout-payment" class="list-view product-checkout" onsubmit="return false;">
                                <div class="payment-type">
                                    <div class="card">
                                        <div class="card-header flex-column align-items-start">
                                            <h4 class="card-title">Payment options</h4>
                                            <p class="card-text text-muted mt-25">Be sure to click on correct payment option</p>
                                        </div>
                                        <div class="card-body">
                                            <h6 id="address-name-final" class="card-holder-name my-75"></h6>
                                            <ul class="other-payment-options list-unstyled">
                                                <p class="text-muted">Expedition Name</p>
                                                @foreach ($deliveries as $delivery)
                                                <li class="py-50">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="delivery{{ $loop->iteration }}" name="delivery" class="custom-control-input" value="{{ $delivery->id }}" />
                                                        <label class="custom-control-label" for="delivery{{ $loop->iteration }}"> {{ $delivery->expedition_name }} </label>
                                                    </div>
                                                </li>
                                                @endforeach
                                            </ul>
                                            <hr class="my-2">
                                            <ul class="other-payment-options list-unstyled">
                                                <p class="text-muted">Payment Method</p>
                                                @foreach ($payments as $payment)
                                                <li class="py-50">
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" id="payment-method{{ $loop->iteration }}" name="payment-method" class="custom-control-input" value="{{ $payment->id }}" />
                                                            <label class="custom-control-label" for="payment-method{{ $loop->iteration }}"> {{ $payment->payment_method }} </label>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <div style="position: relative" >
                                                <button type="button" id="btn-pay" style="position: relative; left: -13px;display: none;"  class="btn btn-primary btn-cvv ml-0 ml-sm-1 mb-50">Continue</button>
                                            </div>
                                            <div class="customer-cvv mt-1" id="payment-price" style="display: none">
                                                <div class="form-inline">
                                                    <label class="mb-50" for="card-holder-cvv">Enter Amount Payable:</label>
                                                    <input type="number" class="form-control ml-sm-75 ml-0 mb-50 input-cvv" name="input-cvv" id="amount-pay" />
                                                    <button type="button" id="pay" class="btn btn-primary btn-cvv ml-0 ml-sm-1 mb-50" >Continue</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="amount-payable checkout-options">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4 class="card-title">Price Details</h4>
                                        </div>
                                        <div class="card-body">
                                            <ul class="list-unstyled price-details">
                                                <li class="price-detail">
                                                    <div class="details-title">Price of {{ $carts->count() }} items</div>
                                                    <div class="detail-amt">
                                                        <strong id="total-p">Rp. {{ number_format($total) }}</strong>
                                                    </div>
                                                </li>
                                                <li class="price-detail">
                                                    <div class="details-title">Delivery Charges</div>
                                                    <div class="font-weight-bold" id="shipping-cost">Rp. 0</div>
                                                </li>
                                            </ul>
                                            <hr />
                                            <ul class="list-unstyled price-details">
                                                <li class="price-detail">
                                                    <div class="details-title">Amount Payable</div>
                                                    <div id="total-py" class="detail-amt font-weight-bolder">Rp. {{ number_format($total) }}</div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                       
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@push('pagecss')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/admin/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/admin/css/pages/app-ecommerce.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/admin/css/plugins/forms/pickers/form-pickadate.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/admin/css/plugins/forms/form-wizard.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/admin/css/plugins/extensions/ext-component-toastr.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets') }}/admin/css/plugins/forms/form-number-input.css">
@endpush
@push('pagejs')
    <script src="{{ asset('assets/admin/js/scripts/pages/app-ecommerce-checkout.js') }}"></script>
@endpush
@push('script')
    <script>
    $(document).on('click','.remove-wishlist', function () {
        var id = $(this).data('id');
        Swal.fire({
            icon: 'error',
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#28C76F',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
        .then((result) => {
            if (result.value) {
                $(this).closest('.ecommerce-card').remove();
                $.ajax({
                    'url': '{{url('cart')}}/' + id,
                    'type': 'POST',
                    'data': {
                        '_method': 'DELETE',
                        '_token': '{{csrf_token()}}',
                    },
                    success: function (response) {
                        if (response == 1) {
                            toastr.error('Data Gagal ditambah!', 'Gagal!', {
                                closeButton: true,
                                tapToDismiss: false
                            });
                        } else {
                            toastr['error']('', 'Removed Item 🗑️', {
                            closeButton: true,
                            tapToDismiss: false
                            });
                        }
                    }
                });
            }
        }); 
  });
    
  $(document).ready(function () {
    let addressID = 0;
    let deliveryID = 0;
    let province = '';
    let city = '';
    let provId = 0;
    let cityId = 0;
        getAddress()
        getProvince()
        $('select[name=province]').on('change', function () {
            if ($(this).find('option[value!=default]')) {
                province = $(this).find(':selected').text()
                provId = $(this).find(':selected').val()
                getCity(provId);
                $('#postal-code').val($(''));
                $(document).find('select[name=city]').on('change', function(){
                    $('#postal-code').val($(this).find(':selected').data('post'));
                    city = $(this).find(':selected').text()
                    cityId = $(this).find(':selected').val()
                })
            }
            
        })
        $('input:radio[name=payment-method]').on('change', function () {
            if($('input:radio[name=payment-method]:checked').val() == 2) {
                $('#payment-price').removeAttr('style');
                $('#btn-pay').attr('style', 'position: relative; left: -13px; display: none;');
            } else {
                $('#payment-price').attr('style', 'display: none;');
                $('#btn-pay').attr('style', 'position: relative; left: -13px;');

            }
        })
        $(document).on('click', '#address-btn', function () {
            var address = $(this).parents('div.card').find('#address-name-init').text();
            $('#address-name-final').text(address);
            addressID = $(this).data('address');
        })
        $('input:radio[name=delivery]').on('change', function () {
            deliveryID = $('input:radio[name=delivery]:checked').val();
            $.ajax({
                type: "POST",
                url: "{{ url('api/cost') }}",
                data: {
                    'delivery': deliveryID,
                    'address': addressID,
                    'user': '{{ Auth::user()->id }}'
                },
                success: function (response) {
                    var shipping = $('#shipping-cost').text('Rp. '+number_format(response.rajaongkir.results[0].costs[0].cost[0].value))
                    var payItems = $('#total-p').text();
                        payItems = payItems.slice(3);
                        payItems = payItems.replace(',', '')
                    const shippingCost = Number(response.rajaongkir.results[0].costs[0].cost[0].value);
                    let total = Number(payItems) + shippingCost;
                        $('#total-py').text('Rp. '+number_format(total))
                }
            });
        })
        $(document).on('click', '#btn-pay', function(){
            var pay = $('#total-py').text();
            pay = pay.slice(3);
            pay = pay.replace(',', '')
            $.ajax({
                type: "POST",
                url: "{{ url('transaction') }}",
                data: {
                    'user': '{{ Auth::user()->id }}',
                    'address': addressID,
                    'delivery': deliveryID,
                    'payment-method': $('input:radio[name=payment-method]:checked').val(),
                    'pay': pay,
                    '_token': '{{ csrf_token() }}'
                },
                success: function (response) {
                    if (response.header == 200) {
                        toastr['success']('', response.message, {
                            closeButton: true,
                            tapToDismiss: true
                            });
                    } else {
                        toastr['error']('', response.message, {
                        closeButton: true,
                        tapToDismiss: true
                        });
                    }
                    window.location('/product');
                }
            });
        })
        $(document).on('click', '#pay', function(){
            $.ajax({
                type: "POST",
                url: "{{ url('transaction') }}",
                data: {
                    'user': '{{ Auth::user()->id }}',
                    'address': addressID,
                    'delivery': $('input:radio[name=delivery]:checked').val(),
                    'payment-method': $('input:radio[name=payment-method]:checked').val(),
                    'pay': $('#amount-pay').val(),
                    '_token': '{{ csrf_token() }}'
                },
                success: function (response) {
                    if (response.header == 200) {
                        toastr['success']('', response.message, {
                            closeButton: true,
                            tapToDismiss: true
                            });
                    } else {
                        toastr['error']('', response.message, {
                        closeButton: true,
                        tapToDismiss: true
                        });
                    }
                    window.location('/product');
                }
            });
        })
        $(document).on('click', '#save', function(){
            $.ajax({
                type: "POST",
                url: "{{ url('api/address') }}",
                data: {
                    "name":  $('#name').val(),
                    "number": $('#number').val(),
                    "province": province,
                    "province_id": provId,
                    "city": city,
                    "city_id": cityId,
                    "postal": $('#postal-code').val(),
                    "user": '{{ @Auth::user()->id }}',
                    'atype': $('#add-type option').val(),
                    '_token': '{{ csrf_token() }}'
                },
                success: function (response) {
                    toastr.success('Saved!', 'Address Saved!', {
                            closeButton: true,
                            tapToDismiss: true
                    });
                    $('#number').val('')
                    $('#name').val('')
                    getAddress()
                }
            });
        })

        // UPDATE BUTTON
        $(document).on('keyup', '#quantitycheckout', function () {
            var id = $(this).data('id');
            var pId = $(this).data('product');
            var qty = $(this).val();
            var thisIs = $(this);

                $.ajax({
                    'url': '{{url('cart')}}/' + id,
                    'type': 'POST',
                    'data': {
                        'id': id,
                        'qty': qty,
                        'type': 3,
                        '_method': 'PUT',
                        '_token': '{{csrf_token()}}',
                    },
                    success: function (data) {
                        $('#total-co').text('Rp. ' + number_format(data.total));
                        $('#total-p').text('Rp. ' + number_format(data.total));
                        $('#total-py').text('Rp. ' + number_format(data.total));
                        $('#total').text('Rp. ' + number_format(data.total));
                        thisIs.parents('div.card').find('h4.item-price').text('Rp. ' + number_format(data.cart.price_total));
                        // thisIs.parents('body').find('h6#price-item').text('Rp. ' + number_format(data.cart.price_total));
                        toastr.success('Data Berhasi ditambah!', 'Berhasil!', {
                            closeButton: true,
                            tapToDismiss: true
                        });
                        
                    }
                    
                });
        });
        // UPDATE INPUT
        $(document).on('change', '#quantitycheckout', function () {
            var id = $(this).data('id');
            var qty = $(this).val();
            var thisIs = $(this);
            $.ajax({
                'url': '{{url('cart')}}/' + id,
                'type': 'POST',
                'data': {
                    'id': id,
                    'qty': qty,
                    'type': 3,
                    '_method': 'PUT',
                    '_token': '{{csrf_token()}}',
                },
                success: function (data) {
                    $('#total-co').text('Rp. ' + number_format(data.total));
                    $('#total-p').text('Rp. ' + number_format(data.total));
                    $('#total-py').text('Rp. ' + number_format(data.total));
                    $('#total').text('Rp. ' + number_format(data.total));
                    thisIs.parents('div.card').find('h4.item-price').text('Rp. ' + number_format(data.cart.price_total));
                    toastr.success('Data Berhasi ditambah!', 'Berhasil!', {
                        closeButton: true,
                        tapToDismiss: true
                    });
                }
            });
        });






        function getProvince(){
            $.ajax({
                type: "GET",
                url: "{{ url('api/province') }}",
                dataType: "JSON",
                success: function (response) {
                    let rows = '';
                    $.each(response.rajaongkir.results, function (idx, raja) { 
                        rows += '<option value="'+raja.province_id+'">'+raja.province+'</option>';
                    });
                    $('#province').append(rows);
                }
            });
        }
        function getCity(provId) {
            $.ajax({
                type: "GET",
                url: "{{ url('api/city') }}/"+provId,
                dataType: "JSON",
                success: function (response) {
                    $(document).find('select[name=city] option[value!=default]').remove()
                    let rows = '';
                    $.each(response.rajaongkir.results, function (idx, raja) { 
                        rows += '<option value="'+raja.city_id+'" data-post="'+raja.postal_code+'">'+raja.type+' '+raja.city_name+'</option>';
                    });
                    $('#city').append(rows);
                },
            });
        }
        function getAddress() {
            $.ajax({
                type: "GET",
                url: "{{ url('api/address') }}",
                dataType: "JSON",
                success: function (response) {
                    let rows = '';
                    if (response.count_of_the_addr == null) {
                        rows    +=  '<div class="customer-card">'+
                                        '<div class="card">'+
                                            '<div class="card-header">'+
                                                '<h4 class="card-title">'+
                                                    'No Address Found!'+
                                                '</h4>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>';
                    }
                    if (response.count_of_the_addr > 0 ) {
                        $.each(response.addresses, function (idx, address) { 
                            rows    +=  '<div class="d-flex" >'+
                                            '<div class="flex-column">'+
                                                '<div class="customer-card">'+
                                                    '<div class="card">'+
                                                        '<div class="card-header">'+
                                                            '<h4 id="address-name-init" class="card-title">'+address.full_name+'</h4>'+
                                                            '<button style="margin:0;" class="badge badge-pill badge-danger"><i data-feather="trash-2"></i></button>'+
                                                            '<button style="margin:0;" class="badge badge-pill badge-warning"><i data-feather="edit-3"></i></button>'+
                                                        '</div>'+
                                                        '<div class="card-body actions">'+
                                                            '<p class="card-text">'+address.city+'</p>'+
                                                            '<p class="card-text">UTC+7: Waktu Indonesia Barat (WIB)</p>'+
                                                            '<p class="card-text mb-0"> Postal Code : '+address.postal_code+'</p>'+
                                                            '<p class="card-text">Phone Number : '+address.phone_number+'</p>'+
                                                            '<button data-address="'+address.id+'" id="address-btn" type="button" class="btn btn-primary btn-block btn-next delivery-address mt-2">'+
                                                                'Deliver To This Address'+
                                                            '</button>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>';
                        });
                    }
                    $('#list-address').html('');
                    $('#list-address').append(rows);
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
                }
            });
        }
    });
    </script>
@endpush

