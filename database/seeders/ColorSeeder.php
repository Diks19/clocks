<?php

namespace Database\Seeders;

use App\Models\ProductColor;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductColor::create([
            'color_name' => 'Red',
            'color_hex' => '#d71515'
        ]);
        ProductColor::create([
            'color_name' => 'Green',
            'color_hex' => '#1feb04'
        ]);
    }
}
