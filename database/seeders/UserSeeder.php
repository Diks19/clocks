<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\Hash;
use App\Models\User;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'andhika1910@gmail.com',
            'password' => Hash::make('asdfqwer'),
            'role' => 'Admin'
        ]);
        User::create([
            'name' => 'User',
            'email' => 'dhikabayu@gmail.com',
            'password' => Hash::make('asdfqwer')
        ]);
    }
}
