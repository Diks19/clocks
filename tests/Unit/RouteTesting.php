<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\Route;
use Tests\TestCase;
// use PHPUnit\Framework\TestCase;

class RouteTesting extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example()
    {
        $data = [
            'destination' => 23,
            'weight' => 2000,
            'courier' =>  'jne',
            // '_token' => csrf_token()
        ];
        $this->post(route('raja.cost', $data))->assertStatus(200);
    }
}
