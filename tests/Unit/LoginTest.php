<?php

namespace Tests\Unit;

use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example()
    {
        $user = [
            'email' => 'andhika1910@gmail.com',
            'password' => 'asdfqwer'
        ];

        $this->post(route('login', $user))->assertStatus(302);
    }
}
