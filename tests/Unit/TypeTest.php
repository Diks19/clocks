<?php

namespace Tests\Unit;

use Tests\TestCase;

class TypeTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_example()
    {
        $type = [
            'type' => 'aku jadi duta sampo lain ? zehahahahahaha',
        ];
        $this->post(route('type.store', $type))->assertStatus(200)->assertJson($type);
    }
}
